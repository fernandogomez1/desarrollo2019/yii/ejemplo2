﻿create database ejemploYii2Desarrollo;

use ejemploYii2Desarrollo;

create or replace table catalogo(
	id int AUTO_INCREMENT,
	nombre varchar(100),
	description varchar(100),
	primary key(id)
);

insert into catalogo values
	(1, 'flor1',' Flor del campo'),
	(2, 'flor2',' Flor del bosque');